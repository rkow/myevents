﻿using Domain.Entities;
using Infrastructure.Identity;
using Microsoft.AspNetCore.Identity;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure.Persistence
{
    public static class ApplicationDbContextSeedData
    {
        private static readonly string userId = Guid.NewGuid().ToString();
        private static readonly string objectId = Guid.NewGuid().ToString();
        public static async Task SeedDefaultUserAsync(UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager, ApplicationDbContext context)
        {
            var userRole = new IdentityRole("User");

            if (roleManager.Roles.All(r => r.Name != userRole.Name))
            {
                await roleManager.CreateAsync(userRole);
            }

            var user = new ApplicationUser { UserName = "user@localhost", Email = "user@localhost", Id = userId};

            if (userManager.Users.All(u => u.UserName != user.UserName))
            {
                var result = await userManager.CreateAsync(user, "User!123456");
                await userManager.AddToRolesAsync(user, new[] { userRole.Name });
                await SeedSampleUserAsync(context, user);
            }
        }

        private static async Task SeedSampleUserAsync(ApplicationDbContext context, ApplicationUser user)
        {
            if (!context.Players.Any())
            {
                context.Players.Add(new User
                {
                    City = "Gdańsk",
                    FullName = user.UserName,
                    Id = user.Id
                });

                await context.SaveChangesAsync();
            }
        }

        public static async Task SeedSampleObjectsAsync(ApplicationDbContext context)
        {
            if (!context.Objects.Any())
            {
                context.Objects.Add(new Domain.Entities.Object
                {
                    Id = objectId,
                    City = "Gdańsk",
                    Name = "Orlik SP 52"
                });

                context.Objects.Add(new Domain.Entities.Object
                {
                    Id = Guid.NewGuid().ToString(),
                    City = "Gdynia",
                    Name = "Gdynia Arena"
                });

                await context.SaveChangesAsync();
            }
        }

        public static async Task SeedSampleEventAsync(ApplicationDbContext context)
        {
            if (!context.Events.Any())
            {
                context.Events.Add(new Event
                {
                    Id = Guid.NewGuid().ToString(),
                    Name = "Mecz Fronted Devs vs Backend Devs",
                    From = DateTime.Now,
                    Duration = TimeSpan.FromMinutes(90),
                    ObjectId = objectId,
                    OwnerId = userId
                });

                await context.SaveChangesAsync();
            }
        }
    }
}
