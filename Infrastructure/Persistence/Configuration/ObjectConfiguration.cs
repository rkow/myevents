﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Persistence.Configuration
{
    public class ObjectConfiguration : IEntityTypeConfiguration<Object>
    {
        public void Configure(EntityTypeBuilder<Object> builder)
        {
            builder.Property(t => t.Name)
                .HasMaxLength(300)
                .IsRequired();

            builder.Property(t => t.City)
                .HasMaxLength(30)
                .IsRequired();
        }
    }
}
