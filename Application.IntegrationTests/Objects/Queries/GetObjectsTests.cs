using Application.Objects.Queries.GetObjects;
using FluentAssertions;
using NUnit.Framework;
using System;
using System.Threading.Tasks;

namespace Application.IntegrationTests.Objects.Queries
{
    using static Testing;
    using Object = Domain.Entities.Object;

    public class GetObjectsTests : TestBase
    {
        [Test]
        public async Task ShouldReturnObjectsByCity()
        {
            await AddAsync(new Object
            {
                Id = Guid.NewGuid().ToString(),
                City = "Gda�sk",
                Name = "Orlik SP 52"
            });

            await AddAsync(new Object
            {
                Id = Guid.NewGuid().ToString(),
                City = "Gdynia",
                Name = "Gdynia Arena"
            });

            var query = new GetObjectsQuery { 
                City = "Gda�sk"
            };

            var result = await SendAsync(query);

            result.Should().HaveCount(1);
        }
    }
}