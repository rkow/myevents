﻿using Application.Events.Command.Create;
using Domain.Entities;
using FluentAssertions;
using NUnit.Framework;
using System;
using System.Threading.Tasks;

namespace Application.IntegrationTests.Events.Commands
{
    using static Testing;
    using Object = Domain.Entities.Object;

    class CreateEventTests : TestBase
    {
        [Test]
        public async Task ShouldCreateEvent()
        {
            var userId = await RunAsDefaultUserAsync("Gdynia");
            var @object = await FindAsync<Object>(obj => obj.City == "Gdynia");

            var command = new CreateEventCommand
            {
                Name = "Mecz Gdynia Arena",
                From = DateTime.Now.AddDays(5),
                Duration = TimeSpan.FromMinutes(90),
                OwnerId = userId,
                ObjectId = @object?.Id
            };

            var id = await SendAsync(command);

            var @event = await FindAsync<Event>(id);

            @event.Should().NotBeNull();
            @event.Name.Should().Be(command.Name);
            @event.CreatedBy.Should().Be(userId);
            @event.CreatedDate.Should().BeCloseTo(DateTime.Now, TimeSpan.FromSeconds(10));
        }
    }
}
