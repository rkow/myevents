﻿using Application.Events.Queries;
using Domain.Entities;
using FluentAssertions;
using NUnit.Framework;
using System;
using System.Threading.Tasks;

namespace Application.IntegrationTests.Events.Queries
{
    using static Testing;
    using Object = Domain.Entities.Object;

    public class GetUserEvents : TestBase
    {
        [Test]
        public async Task ShouldReturnEmptyList()
        {
            var userId = await RunAsDefaultUserAsync("Gdynia");

            await AddAsync(new Event
            {
                Id = Guid.NewGuid().ToString(),
                Name = "Piłka na Wielkim Kacku",
                Duration = TimeSpan.FromMinutes(90),
                From = DateTime.Now.AddDays(3),
                Object = new Domain.Entities.Object
                {
                    Id = Guid.NewGuid().ToString(),
                    City = "Gdynia",
                    Name = "Boisko przy szkole morskiej"
                },
                OwnerId = userId
            });

            var query = new GetUserEventsQuery { OwnerId = Guid.NewGuid().ToString() };

            var result = await SendAsync(query);

            result.Should().BeEmpty();
        }

        [Test]
        public async Task ShouldReturnEventCreatedByUser()
        {
            var userId = await RunAsDefaultUserAsync("Gdynia");
            var @object = await FindAsync<Object>(obj => obj.City == "Gdynia");

            await AddAsync(new Event
            {
                Id = Guid.NewGuid().ToString(),
                Name = "Piłka na Wielkim Kacku",
                Duration = TimeSpan.FromMinutes(90),
                From = DateTime.Now.AddDays(3),
                ObjectId = @object?.Id,
                OwnerId = userId
            });

            var query = new GetUserEventsQuery { OwnerId = userId };

            var result = await SendAsync(query);

            result.Should().HaveCount(1);
        }
    }
}
