﻿using Application.Events.Queries;
using Domain.Entities;
using FluentAssertions;
using NUnit.Framework;
using System;
using System.Threading.Tasks;

namespace Application.IntegrationTests.Events.Queries
{
    using static Testing;

    public class GetEventsTests : TestBase
    {
        [Test]
        public async Task ShouldReturnEmptyList()
        {
            var query = new GetEventsQuery();

            var result = await SendAsync(query);

            result.Should().BeEmpty();
        }

        [Test]
        public async Task ShouldReturnEvents()
        {
            await AddAsync(new Event
            {
                Id = Guid.NewGuid().ToString(),
                Name = "Piłka na Wielkim Kacku",
                Duration = TimeSpan.FromMinutes(90),
                From = DateTime.Now.AddDays(3),
                Object = new Domain.Entities.Object
                {
                    Id = Guid.NewGuid().ToString(),
                    City = "Gdynia",
                    Name = "Boisko przy szkole morskiej"
                }
            });

            var query = new GetEventsQuery();

            var result = await SendAsync(query);

            result.Should().NotBeEmpty();
        }
    }
}
