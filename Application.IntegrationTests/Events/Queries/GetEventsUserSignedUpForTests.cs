﻿using Application.Events.Queries;
using Domain.Entities;
using FluentAssertions;
using NUnit.Framework;
using System;
using System.Threading.Tasks;

namespace Application.IntegrationTests.Events.Queries
{
    using static Testing;
    using Object = Domain.Entities.Object;

    public class GetEventsUserSignedUpForTests : TestBase
    {
        [Test]
        public async Task ShouldReturnEventUserSignedUpFor()
        {
            var userId = await RunAsDefaultUserAsync("Gdynia");
            var @object = await FindAsync<Object>(obj => obj.City == "Gdynia");
            var @event = new Event
            {
                Id = Guid.NewGuid().ToString(),
                Name = "Piłka na Wielkim Kacku",
                Duration = TimeSpan.FromMinutes(90),
                From = DateTime.Now.AddDays(3),
                ObjectId = @object?.Id,
                OwnerId = userId
            };

            @event.EventMembers.Add(new EventMember {
                MemberId = userId,
                Id = Guid.NewGuid().ToString()
            });

            await AddAsync(@event);

            var query = new GetEventsUserSignedUpForQuery { UserId = userId };

            var result = await SendAsync(query);

            result.Should().HaveCount(1);
        }
    }
}
