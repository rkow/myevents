import { EventMember } from "./event-member";
import { Object } from "./object";

export class Event {
    public id: string;
    public name: string;
    public from: Date;
    public duration: string;
    public ownerId: string;
    public object: Object;
    public eventMembers: EventMember[];
}