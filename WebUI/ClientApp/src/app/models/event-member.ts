import { User } from "oidc-client";

export class EventMember {
    public id: string;
    public user: User;
}