import { HttpClient } from '@angular/common/http';
import { Component, Inject, Input } from '@angular/core';
import { Event } from "../../models/event";

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
})
export class EventComponent {

  @Input()
  events: Event[];

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    http.get<Event[]>(baseUrl + 'api/events').subscribe(result => {
      this.events = result;
    }, error => console.error(error));
  }
}
