import { HttpClient } from '@angular/common/http';
import { Component, Inject } from '@angular/core';

@Component({
    selector: 'app-all-events',
    templateUrl: './all-events.component.html'
})
export class AllEventsComponent {

    events: Event[];

    constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
        http.get<Event[]>(baseUrl + 'api/events').subscribe(result => {
            this.events = result;
        }, error => console.error(error));
    }
}
