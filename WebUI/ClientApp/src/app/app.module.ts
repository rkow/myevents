import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { EventComponent } from './components/event/event.component';
import { EventsSignedForMeComponent } from './events-signed-for-me/events-signed-for-me.component';
import { MyEventComponent } from './my-event/my-event.component';
import { ApiAuthorizationModule } from 'src/api-authorization/api-authorization.module';
import { AuthorizeGuard } from 'src/api-authorization/authorize.guard';
import { AuthorizeInterceptor } from 'src/api-authorization/authorize.interceptor';
import { AllEventsComponent } from './all-events/all-events.component';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    EventComponent,
    EventsSignedForMeComponent,
    MyEventComponent,
    AllEventsComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    ApiAuthorizationModule,
    RouterModule.forRoot([
      { path: '', component: AllEventsComponent, pathMatch: 'full' },
      { path: 'events-signed-for-me', component: EventsSignedForMeComponent },
      { path: 'my-events', component: MyEventComponent, canActivate: [AuthorizeGuard] },
    ])
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthorizeInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
