import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-my-event',
  templateUrl: './my-event.component.html'
})
export class MyEventComponent {
  events: Event[];

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
      http.get<Event[]>(baseUrl + `api/events/my`).subscribe(result => {
          this.events = result;
      }, error => console.error(error));
  }
}

