import { HttpClient } from '@angular/common/http';
import { Component, Inject } from '@angular/core';

@Component({
  selector: 'app-events-signed-for-me',
  templateUrl: './events-signed-for-me.component.html'
})
export class EventsSignedForMeComponent {
  events: Event[];

    constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
        http.get<Event[]>(baseUrl + 'api/events/assigned-me').subscribe(result => {
            this.events = result;
        }, error => console.error(error));
    }
}
