﻿using Application.EventMembers.Command.Create;
using Application.EventMembers.Command.Delete;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace WebUI.Controllers
{
    [Authorize]
    public class RegistrationForEventsController : ApiBaseController
    {
        [HttpPost]
        public async Task<ActionResult<string>> Create(CreateEventMemberCommand command)
        {
            return await Mediator.Send(command);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(string id)
        {
            await Mediator.Send(new DeleteEventMemberCommand { Id = id });

            return NoContent();
        }
    }
}
