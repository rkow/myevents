﻿using Application.Objects.Queries.GetObjects;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WebUI.Controllers
{
    [Authorize]
    public class ObjectsController : ApiBaseController
    {
        [HttpGet]
        public async Task<IList<ObjectDto>> Get()
        {
            return await Mediator.Send(new GetObjectsQuery());
        }
    }
}
