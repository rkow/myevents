﻿using Application.Common.Interfaces;
using Application.Events.Command.Create;
using Application.Events.Command.Delete;
using Application.Events.Command.Update;
using Application.Events.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WebUI.Controllers
{
    [Authorize]
    public class EventsController : ApiBaseController
    {
        private readonly ICurrentUserService currentUserService;

        public EventsController(ICurrentUserService currentUserService)
        {
            this.currentUserService = currentUserService;
        }

        [HttpGet]
        public async Task<IList<EventDto>> Get()
        {
            return await Mediator.Send(new GetEventsQuery());
        }

        [HttpGet("my")]
        public async Task<IList<EventDto>> GetUsersEvents()
        {
            return await Mediator.Send(new GetUserEventsQuery { OwnerId = currentUserService.UserId});
        }

        [HttpGet("assigned-me")]
        public async Task<IList<EventDto>> GetAssignedForUserEvents()
        {
            return await Mediator.Send(new GetEventsUserSignedUpForQuery { UserId = currentUserService.UserId });
        }

        [HttpPost]
        public async Task<ActionResult<string>> Create(CreateEventCommand command)
        {
            return await Mediator.Send(command);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> Update(string id, EventUpdateCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }

            await Mediator.Send(command);

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(string id)
        {
            await Mediator.Send(new DeleteEventCommand { Id = id });

            return NoContent();
        }
    }
}
