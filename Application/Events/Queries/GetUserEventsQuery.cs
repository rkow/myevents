﻿using Application.Common.Interfaces;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Events.Queries
{
    public class GetUserEventsQuery : IRequest<IList<EventDto>>
    {
        public string OwnerId { get; set; }
    }

    public class GetUserEventsQueryHandler : IRequestHandler<GetUserEventsQuery, IList<EventDto>>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetUserEventsQueryHandler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<IList<EventDto>> Handle(GetUserEventsQuery request, CancellationToken cancellationToken)
        {
            return await _context.Events
                .Where(@event => @event.OwnerId == request.OwnerId)
                .OrderBy(@event => @event.From)
                .ProjectTo<EventDto>(_mapper.ConfigurationProvider)
                .ToListAsync();
        }
    }
}
