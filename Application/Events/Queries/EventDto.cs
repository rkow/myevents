﻿using Application.Common.Mappings;
using Domain.Entities;
using System;
using System.Collections.Generic;

namespace Application.Events.Queries
{
    public class EventDto : IMapFrom<Event>
    {
        public EventDto()
        {
            EventMembers = new List<EventMemberDto>();
        }

        public string Id { get; set; }
        public string Name { get; set; }
        public DateTime From { get; set; }
        public TimeSpan Duration { get; set; }
        public string OwnerId { get; set; }
        public ObjectDto Object { get; set; }
        public IList<EventMemberDto> EventMembers { get; set; }
    }
}
