﻿using Application.Common.Mappings;
using Object = Domain.Entities.Object;

namespace Application.Events.Queries
{
    public class ObjectDto: IMapFrom<Object>
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
    }
}
