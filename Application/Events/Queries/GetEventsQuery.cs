﻿using Application.Common.Interfaces;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Events.Queries
{
    public class GetEventsQuery: IRequest<IList<EventDto>>
    {
    }

    public class GetEventsQueryHandler : IRequestHandler<GetEventsQuery, IList<EventDto>>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetEventsQueryHandler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<IList<EventDto>> Handle(GetEventsQuery request, CancellationToken cancellationToken)
        {
            return await _context.Events
                .OrderBy(@event => @event.From)
                .ProjectTo<EventDto>(_mapper.ConfigurationProvider)
                .ToListAsync();
        }
    }
}
