﻿using Application.Common.Mappings;
using Domain.Entities;

namespace Application.Events.Queries
{
    public class EventMemberDto: IMapFrom<EventMember>
    {
        public string Id { get; set; }
        public UserDto Member { get; set; }
    }
}
