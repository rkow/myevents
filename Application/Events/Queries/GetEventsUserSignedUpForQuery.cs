﻿using Application.Common.Interfaces;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Events.Queries
{
    public class GetEventsUserSignedUpForQuery: IRequest<IList<EventDto>>
    {
        public string UserId { get; set; }
    }

    public class GetEventsUserSignedUpForQueryHandler : IRequestHandler<GetEventsUserSignedUpForQuery, IList<EventDto>>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetEventsUserSignedUpForQueryHandler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<IList<EventDto>> Handle(GetEventsUserSignedUpForQuery request, CancellationToken cancellationToken)
        {
           return await _context.Events
                .Where(@event => @event.EventMembers.Any(eventMember => eventMember.MemberId == request.UserId))
                .ProjectTo<EventDto>(_mapper.ConfigurationProvider)
                .ToListAsync();
        }
    }
}
