﻿using Application.Common.Mappings;
using Domain.Entities;

namespace Application.Events.Queries
{
    public class UserDto: IMapFrom<User>
    {
        public string Id { get; set; }
        public string FullName { get; set; }
        public string City { get; set; }
    }
}
