﻿using Application.Common.Exceptions;
using Application.Common.Interfaces;
using Domain.Entities;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Events.Command.Update
{
    public class EventUpdateCommand: IRequest
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public DateTime From { get; set; }
        public TimeSpan Duration { get; set; }
        public string ObjectId { get; set; }
    }

    public class EventUpdateCommandHandler : IRequestHandler<EventUpdateCommand>
    {
        private readonly IApplicationDbContext _context;

        public EventUpdateCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Unit> Handle(EventUpdateCommand request, CancellationToken cancellationToken)
        {
            var entity = await _context.Events.FindAsync(request.Id);

            if (entity == null)
            {
                throw new NotFoundException(nameof(Event), request.Id);
            }

            entity.Name = request.Name;
            entity.From = request.From;
            entity.Duration = request.Duration;
            entity.ObjectId = request.ObjectId;

            await _context.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
