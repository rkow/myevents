﻿using Application.Common.Interfaces;
using Domain.Entities;
using Domain.Events;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Events.Command.Create
{
    public class CreateEventCommand: IRequest<string>
    {
        public string Name { get; set; }
        public DateTime From { get; set; }
        public TimeSpan Duration { get; set; }
        public string OwnerId { get; set; }
        public string ObjectId { get; set; }
    }

    public class CreateEventCommandHandler : IRequestHandler<CreateEventCommand, string>
    {
        private readonly IApplicationDbContext _context;

        public CreateEventCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<string> Handle(CreateEventCommand request, CancellationToken cancellationToken)
        {
            var entity = new Event
            {
                Id = Guid.NewGuid().ToString(),
                Name = request.Name,
                From = request.From,
                Duration = request.Duration,
                OwnerId = request.OwnerId,
                ObjectId = request.ObjectId
            };

            entity.DomainEvents.Add(new EventCreatedEvent(entity));

            _context.Events.Add(entity);

            await _context.SaveChangesAsync(cancellationToken);

            return entity.Id;
        }
    }
}
