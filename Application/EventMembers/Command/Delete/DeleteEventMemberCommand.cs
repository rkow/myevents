﻿using Application.Common.Exceptions;
using Application.Common.Interfaces;
using Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Application.EventMembers.Command.Delete
{
    public class DeleteEventMemberCommand: IRequest
    {
        public string Id { get; set; }
    }

    public class DeleteEventMemberCommandHandler : IRequestHandler<DeleteEventMemberCommand>
    {
        private readonly IApplicationDbContext _context;

        public DeleteEventMemberCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Unit> Handle(DeleteEventMemberCommand request, CancellationToken cancellationToken)
        {
            var entity = await _context.EventMembers
                .Where(l => l.Id == request.Id)
                .SingleOrDefaultAsync(cancellationToken);

            if (entity == null)
            {
                throw new NotFoundException(nameof(EventMember), request.Id);
            }

            _context.EventMembers.Remove(entity);

            await _context.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
