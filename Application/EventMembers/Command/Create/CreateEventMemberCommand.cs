﻿using Application.Common.Exceptions;
using Application.Common.Interfaces;
using Domain.Entities;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.EventMembers.Command.Create
{
    public class CreateEventMemberCommand: IRequest<string>
    {
        public string MemberId { get; set; }
        public string EventId { get; set; }
    }

    public class CreateTodoListCommand : IRequest<string>
    {
        public string Title { get; set; }
    }

    public class CreateEventMemberCommandHandler : IRequestHandler<CreateEventMemberCommand, string>
    {
        private readonly IApplicationDbContext _context;

        public CreateEventMemberCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<string> Handle(CreateEventMemberCommand request, CancellationToken cancellationToken)
        {
            var @event = await _context.Events.FindAsync(request.EventId);

            if(@event == null)
            {
                throw new NotFoundException(nameof(Event), request.EventId.ToString());
            }

            var eventMember = new EventMember
            {
                Id = Guid.NewGuid().ToString(),
                MemberId = request.MemberId
            };

            @event.EventMembers.Add(eventMember);

            await _context.SaveChangesAsync(cancellationToken);

            return eventMember.Id;
        }
    }
}
