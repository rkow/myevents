﻿using Application.Common.Interfaces;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Objects.Queries.GetObjects
{
    public class GetObjectsQuery : IRequest<IList<ObjectDto>>
    {
        public string City { get; set; }
    }

    public class GetObjectsQueryHandler : IRequestHandler<GetObjectsQuery, IList<ObjectDto>>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetObjectsQueryHandler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<IList<ObjectDto>> Handle(GetObjectsQuery request, CancellationToken cancellationToken)
        {
            return await _context.Objects
                .Where(@object => @object.City == request.City)
                .ProjectTo<ObjectDto>(_mapper.ConfigurationProvider)
                .ToListAsync();
        }
    }
}
