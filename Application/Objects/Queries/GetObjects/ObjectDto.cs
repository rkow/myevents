﻿using Application.Common.Mappings;
using Object = Domain.Entities.Object;

namespace Application.Objects.Queries.GetObjects
{
    public class ObjectDto: IMapFrom<Object>
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
