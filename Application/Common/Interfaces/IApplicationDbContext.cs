﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;
using Object = Domain.Entities.Object;

namespace Application.Common.Interfaces
{
    public interface IApplicationDbContext
    {
        DbSet<Event> Events { get; set; }

        DbSet<Object> Objects { get; set; }

        DbSet<EventMember> EventMembers { get; set; }
        DbSet<User> Players { get; set; }

        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}
