﻿using Domain.Entities;
using System;

namespace Domain.Events
{
    public class EventRegistrationEvent
    {
        public EventRegistrationEvent(Event @event, Guid userId)
        {
            Event = @event;
            UserId = userId;
        }

        public Event Event { get; }
        public Guid UserId { get; }
    }
}
