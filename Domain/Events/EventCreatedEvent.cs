﻿using Domain.Common;
using Domain.Entities;

namespace Domain.Events
{
    public class EventCreatedEvent : DomainEvent
    {
        public EventCreatedEvent(Event @event)
        {
            Event = @event;
        }

        public Event Event { get; }
    }
}
