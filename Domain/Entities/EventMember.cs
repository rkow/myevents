﻿using Domain.Common;

namespace Domain.Entities
{
    public class EventMember: AuditableEntity
    {
        public string Id { get; set; }
        public virtual User Member { get; set; }
        public string MemberId { get; set; }
    }
}
