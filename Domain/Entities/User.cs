﻿namespace Domain.Entities
{
    public class User
    {
        public string Id { get; set; }
        public string FullName { get; set; }
        public string City { get; set; }
    }
}
