﻿using Domain.Common;

namespace Domain.Entities
{
    public class Object: AuditableEntity
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
    }
}
