﻿using Domain.Common;
using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public class Event: AuditableEntity, IDomainEvent
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public DateTime From { get; set; }
        public TimeSpan Duration { get; set; }
        public User Owner { get; set; }
        public string OwnerId { get; set; }
        public Object Object { get; set; }
        public string ObjectId { get; set; }
        public List<EventMember> EventMembers { get; private set; } = new List<EventMember>();

        public List<DomainEvent> DomainEvents { get; set; } = new List<DomainEvent>();
    }
}
